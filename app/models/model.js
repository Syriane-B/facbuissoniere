const sql = require("./db.js");

const Ufr = function(ufr) {
  this.label = ufr.ufr_label;
};


Ufr.getAllUfrs = result => {
  sql.query("SELECT * FROM ufr", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("ufrs: ", res);
    result(null, res);
  });
};

const Specialite = function(specialite) {
  this.label = specialite.specialite_label;
  this.ufr = specialite.ufr_id;
};

Ufr.findUfrById = (ufrId, result) => {
  let data = {'ufr_label':'', 'ufr_specialites': []};
  sql.query(`SELECT * FROM ufr WHERE ufr_id = ${ufrId}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    if (res.length) {
      const result_ufr = res[0];
      data.ufr_label = result_ufr.ufr_label;
      sql.query(`SELECT * FROM specialite WHERE specialite.ufr_id = ${ufrId}`, (error, resu) => {
        if (error) {
          console.log("error: ", error);
          result(error, null);
          return;
        }
        if (resu.length) {
          data.ufr_specialites = resu;
        }
        result(null, data);
      });
      return;
    }
    // ufr not found with the id
    result({ kind: "not_found" }, null);
  });
};

Specialite.findSpecialiteById = (speId, result) => {
  let data2 = {'specialite_label': '', 'specialite_appels': [] };
  sql.query(`SELECT * FROM specialite WHERE specialite_id = ${speId}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    if (res.length) {
      const result_spe = res[0];
      data2.specialite_label = result_spe.specialite_label;
      sql.query(`SELECT * FROM appel WHERE appel.specialite_id = ${speId}`, (error, resu) => {
        if (error) {
          console.log("error: ", error);
          result(error, null);
          return;
        }
        if (resu.length) {
          data2.specialite_appels = resu;
        }
        result(null, data2);
      });
      return;
    }
    // spe not found with the id
    result({ kind: "not_found" }, null);
  });
};

Specialite.getAllSpecialites = result => {
  sql.query("SELECT * FROM specialite", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("specialites: ", res);
    result(null, res);
  });
};

module.exports = {
  Ufr,
  Specialite
}

