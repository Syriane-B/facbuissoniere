const model = require("../models/model.js");
console.log(model)

  exports.findAllUfrs = (req, res) => {
    model.Ufr.getAllUfrs((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving ufrs."
        });
      else res.send(data);
    });
  };

  exports.findOneUfr = (req, res) => {
    model.Ufr.findUfrById(req.params.ufrId, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found ufr with id ${req.params.ufrId}.`
          });
        } else {
          res.status(500).send({
            message: "Error retrieving ufr with id " + req.params.ufrId
          });
        }
      } else res.send(data);
    });
  };

  exports.findAllSpecialites = (req, res) => {
    model.Specialite.getAllSpecialites((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving specialites."
        });
      else res.send(data);
    });
  };

  exports.findOneSpecialite = (req, res) => {
    model.Specialite.findSpecialiteById(req.params.speId, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found ufr with id ${req.params.speId}.`
          });
        } else {
          res.status(500).send({
            message: "Error retrieving ufr with id " + req.params.speId
          });
        }
      } else res.send(data);
    });
  };
