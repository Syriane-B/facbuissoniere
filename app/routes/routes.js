module.exports = app => {
    const controller = require("../controllers/controller.js");

    // Retrieve all 
    app.get("/ufrs", controller.findAllUfrs);
    app.get("/specialites", controller.findAllSpecialites);
  
    // Retrieve a single ufr with Id and the list of specialities
    app.get("/ufrs/:ufrId", controller.findOneUfr);

    app.get("/specialites/:speId", controller.findOneSpecialite);
};