const express = require("express");
const cors = require('cors');
const dotenv = require('dotenv');
dotenv.config();

const app = express();
// CORS policy setup
app.use(cors({
  origin: '*'
}));


// parse requests of content-type: application/json
app.use(express.json());

// parse requests of content-type: application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to our application." });
});

require("./app/routes/routes.js")(app);

// set port, listen for requests
app.listen(3000, () => {
  console.log("Server is running on port 3000.");
});
