let $requestTitle = document.getElementById('request-title');
let $requestResult = document.getElementById('requestResult');

fetch('http://localhost:3000/ufrs')
    .then((resp) => resp.json())
    .then(function(data) {
        let ufrs = data;
        $requestResult.innerHTML = '';
        return ufrs.map(function(ufr) {
            let $ufrItem = document.createElement('button');
            $ufrItem.classList.add('btn');
            $ufrItem.classList.add('btn-secondary');
            $ufrItem.classList.add('m-1');
            $ufrItem.innerText = ufr.ufr_label;
            $ufrItem.addEventListener('click', (e) => {
                e.preventDefault();
                showSpecialites(ufr.ufr_id);
            })
            $requestResult.append($ufrItem);
        })
    })
    .catch(function(error) {
        console.log(error);
    });

function showSpecialites(ufrId) {
    fetch('http://localhost:3000/ufrs/' + ufrId)
        .then((resp) => resp.json())
        .then(function(data) {
            console.log(data);
            let specialities = data.ufr_specialites
            $requestTitle.innerText = 'Nom de l\'ufr : ' + data.ufr_label;
            $requestResult.innerHTML = '';
            return specialities.map(function(speciality) {
                let $specialityItem = document.createElement('button');
                $specialityItem.classList.add('btn');
                $specialityItem.classList.add('btn-secondary');
                $specialityItem.classList.add('m-1');
                $specialityItem.innerText = speciality.specialite_label;
                $specialityItem.addEventListener('click', (e) => {
                    e.preventDefault();
                    $requestResult.innerHTML = '';
                    console.log(speciality.specialite_id);

                    showSpecialite(speciality.specialite_id);
                    console.log(speciality.specialite_id)
                })
                $requestResult.append($specialityItem);
            })
        })
        .catch(function(error) {
            console.log(error);
        });

}

function showSpecialite(specialiteId) {
    fetch('http://localhost:3000/specialites/' + specialiteId)
        .then((resp) => resp.json())
        .then(function(data) {
            console.log('data');
            console.log(data);
            let specialite_appels = data.specialite_appels
            $requestTitle.innerText = 'Nom de la specialité : ' + data.specialite_label;
            $requestResult.innerHTML = '';
            return specialite_appels.map(function(appel) {
                console.log(appel);
                let $specialityItem = document.createElement('button');
                $specialityItem.classList.add('btn');
                $specialityItem.classList.add('btn-secondary');
                $specialityItem.classList.add('m-1');
                $specialityItem.innerText = appel.course_label;
                $specialityItem.addEventListener('click', (e) => {
                    e.preventDefault();
                })
                $requestResult.append($specialityItem);
            })
        })
        .catch(function(error) {
            console.log(error);
        });

}
