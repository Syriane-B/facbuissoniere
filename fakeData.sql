INSERT INTO notification_choice 
    (notification_label)
VALUES
    ("Par SMS"),
    ("Par E-Mail"),
    ("Par courrier postale")
;

INSERT INTO app_user
    (first_name, last_name, email, phone, notification_choice)
VALUES
    ("Firstname1", "Lastname1", "email1@domaine.ext", "06 06 06 06 06", 1),
    ("Firstname2", "Lastname2", "email2@domaine.ext", NULL, 2),
    ("Firstname3", "Lastname3", "email3@domaine.ext", NULL, 3),
    ("Firstname4", "Lastname4", "email4@domaine.ext", NULL, 2),
    ("Firstname5", "Lastname5", "email5@domaine.ext", "+33 06 00 00 00", 2),
    ("Firstname6", "Lastname6", "email6@domaine.ext", NULL, 2),
    ("Firstname7", "Lastname7", "email7@domaine.ext", NULL, 2),
    ("Firstname8", "Lastname8", "email8@domaine.ext", NULL, 2),
    ("Firstname9", "Lastname9", "email9@domaine.ext", NULL, 2)
;

INSERT INTO authorization 
    (authorization_label)
VALUES
    ("Lean Dev WIP")
;

INSERT INTO user_authorization 
    (user_id, authorization_id)
VALUES
    (1, 1),
    (2, 1),
    (3, 1)
;

-- ### Scolarity Part ###
INSERT INTO ufr 
    (ufr_label)
VALUES
    ("UFR Exemple 1"),
    ("UFR Exemple 2")
;

INSERT INTO specialite 
    (specialite_label, ufr_id)
VALUES
    ("Spécialité A", 1),
    ("Spécialité B", 1),
    ("Spécialité C", 2)
;

-- ### Students Part ###
INSERT INTO promo 
    (specialite_id, promo_label)
VALUES
    (1, "Promo1A"),
    (1, "Promo1B"),
    (2, "Promo2A"),
    (3, "Promo3A")
;

INSERT INTO student_status
    (status_label)
VALUES
    ("Assidu"),
    ("Absentéiste"),
    ("Démissionaire")
;

INSERT INTO student
    (student_nb, user_id, status_id, status_date, entry_date)
VALUES
    ("2O21A00001", 1, 1, '2021-01-01', '2021-01-01'),
    ("2O21A00002", 2, 1, '2021-01-01', '2021-01-01'),
    ("2O21A00003", 3, 2, '2021-01-01', '2021-01-01'),
    ("2O21A00004", 4, 3, '2021-01-01', '2021-01-01'),
    ("2O21A00005", 5, 1, '2021-01-01', '2021-01-01'),
    ("2O21A00006", 6, 1, '2021-01-01', '2021-01-01')
;
 
INSERT INTO student_promo 
    (student_nb, promo_id)
VALUES
    ("2O21A00001", 1),
    ("2O21A00002", 1),
    ("2O21A00003", 1),
    ("2O21A00003", 2),
    ("2O21A00004", 2),
    ("2O21A00005", 3),
    ("2O21A00006", 4)
;

-- ### Scolarity Admin Part ###
INSERT INTO scolar_admin
    (ufr_id, user_id)
VALUES
    (1, 7),
    (2, 7),
    (2, 6)
;

-- ### Prof Part ###
INSERT INTO prof
    (user_id, specialite_id)
VALUES
    (8, 1),
    (9, 2),
    (9, 3)
;

-- ### Appel Part ###
INSERT INTO appel_status
    (status_label)
VALUES
    ("Programmé"),
    ("En cours"),
    ("Terminé")
;

INSERT INTO appel 
    (course_label, specialite_id, prof_id, appel_time, appel_day, status_id)
VALUES
    ("Titre du cours 1", 1, 1, '2021-01-01 08:00:00', '2021-01-01', 1),
    ("Titre du cours 2", 2, 2, '2021-01-01 08:00:00', '2021-01-01', 1)
;

INSERT INTO student_appel 
    (appel_id, student_nb, presence)
VALUES
    (1, "2O21A00001", TRUE),
    (1, "2O21A00002", TRUE),
    (1, "2O21A00003", TRUE),
    (2, "2O21A00004", TRUE),
    (2, "2O21A00005", FALSE),
    (2, "2O21A00006", TRUE),
    (2, "2O21A00001", FALSE),
    (2, "2O21A00002", TRUE),
    (2, "2O21A00003", FALSE)
;

-- ### Justif Part ###
INSERT INTO justif_status 
    (status_label)
VALUES
    ("En attente"),
    ("A valider"),
    ("Refusé"),
    ("Validé")
;

INSERT INTO justif 
    (absence_id, justif_date, status_id, date_status_udapte)
VALUES
    (4, '2021-01-01', 2, '2021-01-01'),
    (6, '2021-01-01', 3, '2021-01-01'),
    (8, '2021-01-01', 1, NULL)
;

INSERT INTO student_appel_justif
    (justif_id, absence_id)
VALUES
    (1, 1),
    (2, 2)
;