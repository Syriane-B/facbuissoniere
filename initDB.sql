CREATE DATABASE IF NOT EXISTS facbuissoniere;

COMMIT;

START TRANSACTION;

CREATE TABLE notification_choice (
    notification_id INT AUTO_INCREMENT NOT NULL,
    notification_label VARCHAR(50),
    PRIMARY KEY (notification_id)
);

CREATE TABLE app_user (
    user_id INT AUTO_INCREMENT NOT NULL,
    first_name VARCHAR(100) NOT NULL,
    last_name VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL,
    phone VARCHAR(20),
    notification_choice INT NOT NULL,
    PRIMARY KEY (user_id),
    FOREIGN KEY (notification_choice) REFERENCES notification_choice (notification_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE authorization (
    authorization_id INT AUTO_INCREMENT NOT NULL,
    authorization_label VARCHAR(100) NOT NULL,
    PRIMARY KEY (authorization_id)
);

CREATE TABLE user_authorization (
    user_id INT NOT NULL,
    authorization_id INT NOT NULL,
    PRIMARY KEY (user_id, authorization_id),
    FOREIGN KEY (user_id) REFERENCES app_user (user_id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (authorization_id) REFERENCES authorization (authorization_id) ON DELETE CASCADE ON UPDATE CASCADE
);

-- ### Scolarity Part ###
CREATE TABLE ufr (
    ufr_id INT AUTO_INCREMENT NOT NULL,
    ufr_label VARCHAR(255),
    PRIMARY KEY (ufr_id)
);

CREATE TABLE specialite (
    specialite_id INT AUTO_INCREMENT NOT NULL,
    specialite_label VARCHAR(255) NOT NULL,
    ufr_id INT NOT NULL,
    PRIMARY KEY (specialite_id),
    FOREIGN KEY (ufr_id) REFERENCES ufr (ufr_id) ON DELETE CASCADE ON UPDATE CASCADE
);

-- ### Students Part ###
CREATE TABLE promo (
    promo_id INT AUTO_INCREMENT NOT NULL,
    specialite_id INT NOT NULL,
    promo_label VARCHAR(255) NOT NULL,
    PRIMARY KEY (promo_id),
    FOREIGN KEY (specialite_id) REFERENCES specialite (specialite_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE student_status (
    status_id INT AUTO_INCREMENT NOT NULL,
    status_label VARCHAR(100) NOT NULL,
    PRIMARY KEY (status_id)
);

CREATE TABLE student (
    student_nb VARCHAR(30) NOT NULL,
    user_id INT NOT NULL,
    status_id INT NOT NULL,
    status_date DATE,
    entry_date DATE NOT NULL DEFAULT CURRENT_DATE,
    PRIMARY KEY (student_nb),
    FOREIGN KEY (user_id) REFERENCES app_user (user_id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (status_id) REFERENCES student_status (status_id) ON DELETE CASCADE ON UPDATE CASCADE
);
 
CREATE TABLE student_promo (
    student_nb VARCHAR(30) NOT NULL,
    promo_id INT NOT NULL,
    PRIMARY KEY (student_nb, promo_id),
    FOREIGN KEY (student_nb) REFERENCES student (student_nb) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (promo_id) REFERENCES promo (promo_id) ON DELETE CASCADE ON UPDATE CASCADE
);

-- ### Scolarity Admin Part ###
CREATE TABLE scolar_admin (
    ufr_id INT NOT NULL,
    user_id INT NOT NULL,
    PRIMARY KEY (ufr_id, user_id),
    FOREIGN KEY (ufr_id) REFERENCES ufr (ufr_id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (user_id) REFERENCES app_user (user_id) ON DELETE CASCADE ON UPDATE CASCADE
);

-- ### Prof Part ###
CREATE TABLE prof (
    prof_id INT AUTO_INCREMENT NOT NULL,
    user_id INT NOT NULL,
    specialite_id INT NOT NULL,
    PRIMARY KEY (prof_id),
    FOREIGN KEY (user_id) REFERENCES app_user (user_id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (specialite_id) REFERENCES specialite (specialite_id) ON DELETE CASCADE ON UPDATE CASCADE
);

-- ### Appel Part ###
CREATE TABLE appel_status (
    status_id INT AUTO_INCREMENT NOT NULL,
    status_label VARCHAR(30),
    PRIMARY KEY (status_id)
);

CREATE TABLE appel (
    appel_id INT AUTO_INCREMENT NOT NULL,
    course_label VARCHAR(255),
    specialite_id INT NOT NULL,
    prof_id INT NOT NULL,
    appel_time DATETIME NOT NULL,
    appel_day DATE NOT NULL,
    status_id INT NOT NULL,
    PRIMARY KEY (appel_id),
    FOREIGN KEY (specialite_id) REFERENCES specialite (specialite_id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (prof_id) REFERENCES prof (prof_id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (status_id) REFERENCES appel_status (status_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE student_appel (
	pk_id INT PRIMARY KEY AUTO_INCREMENT,
    appel_id INT NOT NULL,
    student_nb VARCHAR(30) NOT NULL,
    presence BOOLEAN DEFAULT TRUE,
    -- PRIMARY KEY (appel_id, student_nb),
    FOREIGN KEY (appel_id) REFERENCES appel (appel_id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (student_nb) REFERENCES student (student_nb) ON DELETE CASCADE ON UPDATE CASCADE
);

-- ### Justif Part ###
CREATE TABLE justif_status (
    status_id INT AUTO_INCREMENT NOT NULL,
    status_label VARCHAR(50),
    PRIMARY KEY (status_id)
);

CREATE TABLE justif (
    justif_id INT AUTO_INCREMENT NOT NULL,
    absence_id INT NOT NULL,
    justif_date DATE DEFAULT CURRENT_DATE,
    status_id INT NOT NULL,
    date_status_udapte DATE,
    PRIMARY KEY (justif_id),
    FOREIGN KEY (absence_id) REFERENCES student_appel (pk_id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (status_id) REFERENCES justif_status (status_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE student_appel_justif (
    justif_id INT NOT NULL,
    absence_id INT NOT NULL,
    PRIMARY KEY (absence_id, justif_id),
    FOREIGN KEY (justif_id) REFERENCES justif (justif_id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (absence_id) REFERENCES appel_status (status_id) ON DELETE CASCADE ON UPDATE CASCADE
);

COMMIT;
