# Facbuissoniere

## Lancer le projet Node

Pour lancer le projet, il est nécessaire de préciser les variables d'environnement en lien avec les informations de connection à la base de données : 

HOST=localhost PORT=3306 USER=syrianeB PASSWORD=New-Brandy-Styled5 DATABASE=facbuissoniere node index.js

Routes de l'api

/ufs -> renvoi la liste des ufs
/ufrs/:id -> renvoie les infos de l'ufr et ses spécialités (nom + id)
/specialités/:id -> renvoie les infos de la spécialité + ses cours (nom + id)
/cours/:id -> renvoie les infos du cours + la liste des étudiants (nom + id)
